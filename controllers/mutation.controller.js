// Importamos el modelo de mutación y el ayudante de mutación.
const Mutation = require("../models/mutation.model");
const mutationHelper = require("../helpers/mutation.helper");

// Definimos un controlador para detectar mutaciones que recibe una solicitud HTTP con una secuencia de ADN en el cuerpo de la solicitud.
exports.detectMutation = async (req, res) => {
  // Obtenemos la secuencia de ADN del cuerpo de la solicitud.
  const dna = req.body.dna;

  // Llamamos a la función hasMutation del ayudante de mutación para determinar si la secuencia de ADN contiene una mutación.
  const mutationResult = mutationHelper.hasMutation(dna);

  // Verificamos si la secuencia de ADN ya existe en la base de datos de MongoDB.
  const existingMutation = await Mutation.findOne({ dna: dna });

  // Si la secuencia de ADN ya existe, devolvemos el resultado guardado en la base de datos.
  if (existingMutation) {
    if (existingMutation.result) {
      res.status(200).send({ success: true, mutationDetected: true });
    } else {
      res.status(403).send({ success: true, mutationDetected: false });
    }
  } else {
    // Si la secuencia de ADN no existe, guardamos el resultado de la detección de mutaciones en la base de datos y devolvemos el resultado.
    const mutation = new Mutation({ dna, result: mutationResult });
    await mutation.save();

    if (mutationResult) {
      res.status(200).send({ success: true, mutationDetected: true });
    } else {
      res.status(403).send({ success: true, mutationDetected: false });
    }
  }
};

// Definimos un controlador de estadísticas que devuelve las estadísticas de las verificaciones de ADN.
exports.getStats = async (_req, res) => {
  // Obtenemos la cantidad de mutaciones y no mutaciones de la base de datos.
  const countMutations = await Mutation.countDocuments();
  const countNoMutation = await Mutation.countDocuments({ result: false });

  // Calculamos la proporción de mutaciones.
  const ratio = countMutations / (countNoMutation || 1);

  // Enviamos la respuesta con las estadísticas.
  res.status(200).json({
    count_mutations: countMutations,
    count_no_mutation: countNoMutation,
    ratio: ratio.toFixed(2),
  });
};