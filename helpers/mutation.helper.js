// Función que verifica si hay una secuencia de cuatro letras iguales en una dirección específica.
const checkDirection = (dna, i, j, di, dj) => {
  const n = dna.length;
  const base = dna[i][j];

  // Comparamos las letras en la dirección especificada.
    for (let k = 1; k < 4; k++) {
    const ni = i + k * di;
    const nj = j + k * dj;

    // Si se encuentra una letra diferente o se sale del rango, no hay secuencia.
    if (ni < 0 || nj < 0 || ni >= n || nj >= n || dna[ni][nj] !== base) {
      return false;
    }
  }

  // Si todas las letras son iguales, se encontró una secuencia.
  return true;
};

// Función que valida si la secuencia de ADN es válida.
const isValidDNA = (dna) => {
  // Verificamos si el ADN es un array y tiene al menos 4 elementos.
  if (!Array.isArray(dna) || dna.length < 4) {
    return false;
  }

  const n = dna.length;
  const regex = /^[ATCG]+$/;

  // Verificamos si cada cadena del array contiene solo letras válidas y tiene la misma longitud que el array.
  for (const strand of dna) {
    if (strand.length !== n || !regex.test(strand)) {
      return false;
    }
  }

  return true;
};

// Función que detecta si una secuencia de ADN tiene una mutación.
const hasMutation = (dna) => {
  // Si el ADN no es válido, no hay mutación.
  if (!isValidDNA(dna)) {
    return false;
  }
  
  const n = dna.length;
  let count = 0;
  // Definimos las direcciones a verificar: horizontal, vertical, diagonal y anti-diagonal.
  const directions = [
    [0, 1],
    [1, 0],
    [1, 1],
    [-1, 1],
  ];

  // Iteramos sobre cada posición del ADN y verificamos las secuencias en todas las direcciones.
  for (let i = 0; i < n; i++) {
    for (let j = 0; j < n; j++) {
      for (const [di, dj] of directions) {
        if (checkDirection(dna, i, j, di, dj)) {
          count++;
          // Si se encuentran más de una secuencia de cuatro letras iguales, hay mutación.
          if (count > 1) {
            return true;
          }
        }
      }
    }
  }

  return false;
};

module.exports = {
  hasMutation,
};