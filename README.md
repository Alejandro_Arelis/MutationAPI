# MutationAPI

Este proyecto es una API que detecta diferencias genéticas basándose en una secuencia de ADN. Si una persona tiene más de una secuencia de cuatro letras iguales en su ADN de forma oblicua (diagonal), horizontal o vertical, se considera que tiene una mutación.


## Requisitos

- Node.js
- MongoDB


## Instalación

1. Clona este repositorio:
git clone https://gitlab.com/Alejandro_Arelis/MutationAPI.git


2. Instala las dependencias del proyecto:
npm install


3. Crea un archivo .env en el directorio raíz del proyecto y configura las variables de entorno:
PORT=8080
MONGODB_URI=<your_mongodb_connection_string>


4. Ejecuta el servidor:
npm start


## Uso

1. Enviar una secuencia de ADN para verificar si hay una mutación:
POST http://localhost:8080/mutation
Content-Type: application/json

{
"dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}

Respuesta HTTP 200-OK si se detecta una mutación, HTTP 403-Forbidden en caso contrario.

2. Obtener estadísticas de las verificaciones de ADN realizadas:
GET http://localhost:8080/stats

Respuesta:
```json
{
  "count_mutations": 40,
  "count_no_mutation": 100,
  "ratio": 0.4
}

Pruebas
Para ejecutar las pruebas, ejecuta el siguiente comando:
npm test

Estructura del proyecto
MutationAPI
├── controllers
│   └── mutation.controller.js
├── database
│   └── connection.js
├── helpers
│   └── mutation.helper.js
├── models
│   └── mutation.model.js
├── routes
│   └── mutation.route.js
├── tests
│   └── mutation.test.js
├── index.js
├── .env
└── .gitignore

Contribuciones
Para contribuir a este proyecto, crea una rama con tus cambios y envía una solicitud de incorporación de cambios (merge request).

Licencia
Este proyecto está bajo la licencia MIT.