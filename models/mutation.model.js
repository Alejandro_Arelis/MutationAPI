const mongoose = require("mongoose");

const mutationSchema = new mongoose.Schema({
  dna: {
    type: [String],
    required: true,
  },
  result: {
    type: Boolean,
    required: true,
  },
});

module.exports = mongoose.model("Mutation", mutationSchema);