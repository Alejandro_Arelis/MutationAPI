const request = require("supertest");
const app = require("../index");
const Mutation = require("../models/mutation.model");

beforeEach(async () => { // Cambia beforeAll a beforeEach
  await Mutation.deleteMany({});
});

// Resto del código ...
test("detect mutation", async () => {
  const response = await request(app)
    .post("/mutation")
    .send({
      dna: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"],
    });

  expect(response.statusCode).toBe(200);
  expect(response.body.success).toBe(true);
  expect(response.body.mutationDetected).toBe(true);
});

test("no mutation", async () => {
  const response = await request(app)
    .post("/mutation")
    .send({
      dna: ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"],
    });

  expect(response.statusCode).toBe(403);
  expect(response.body.success).toBe(true);
  expect(response.body.mutationDetected).toBe(false);
});

test("get stats", async () => {
  const response = await request(app).get("/stats");

  expect(response.statusCode).toBe(200);
  expect(response.body.count_mutations).toBe(1);
  expect(response.body.count_no_mutation).toBe(1);
  expect(response.body.ratio).toBe("1.00");
});