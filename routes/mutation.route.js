const express = require("express");
const router = express.Router();
const mutationController = require("../controllers/mutation.controller");

router.post("/mutation", mutationController.detectMutation);
router.get("/stats", mutationController.getStats);

module.exports = router;