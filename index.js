require('dotenv').config();
const morgan = require("morgan");
const express = require("express");
const connectDB = require("./database/connection");
const mutationRoutes = require("./routes/mutation.route");

const app = express();
const PORT = process.env.PORT || 8080;

connectDB();

app.use(express.json());
app.use("/", mutationRoutes);
app.use(morgan("combined")); 

const server = app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});

module.exports = server;